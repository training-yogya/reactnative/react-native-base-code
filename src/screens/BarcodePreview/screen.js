import React from 'react';
import {AppContainer} from '../../components';
import QRCode from 'react-native-qrcode-svg';
export default function Screen({route}) {
  const value = route.params.value;

  return (
    <AppContainer>
      <QRCode value={value} size={200} />
    </AppContainer>
  );
}
