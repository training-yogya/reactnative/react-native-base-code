import {connect} from 'react-redux';
import {addStore} from '../../features/StoreListRedux/redux/actions';
import Screen from './screen';

const mapStateToProps = (state) => ({
  addLoading: state.store.addLoading,
});

const mapDispatchToProps = (dispatch) => ({
  addStore: (data) => dispatch(addStore(data)),
});

const withRedux = connect(mapStateToProps, mapDispatchToProps);

const StoreAddScreen = withRedux(Screen);

export default StoreAddScreen;
