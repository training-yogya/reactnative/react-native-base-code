import React from 'react';
import {Alert} from 'react-native';
import {AppContainer} from '../../components';
import screenNames from '../../features/StoreListRedux/navigation/screenNames';

import StoreAddForm from './components/StoreAddForm';

export default function Screen({navigation, route, loading, addStore}) {
  const onSubmit = (form) => {
    addStore(form)
      .then((message) => {
        Alert.alert('Berhasil', message, [
          {
            text: 'Ok',
            onPress: () => {
              navigation.navigate(screenNames.list, {
                refresh: true,
              });
            },
          },
        ]);
      })
      .catch((error) => {
        const message = error.message;
        Alert.alert('Error', message);
      });
  };

  return (
    <AppContainer>
      <StoreAddForm onSubmit={onSubmit} loading={loading} />
    </AppContainer>
  );
}
