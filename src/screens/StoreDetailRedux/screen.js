import React, {useLayoutEffect, useState} from 'react';
import {Alert, View} from 'react-native';
import {Card, IconButton, Paragraph, Title} from 'react-native-paper';
import {AppButton, AppContainer} from '../../components';
import screenNames from '../../features/StoreListRedux/navigation/screenNames';
import {DefaultTheme} from '../../theme';

export default function Screen({
  route,
  navigation,
  detail,
  deleteStoreById,
  loading,
}) {
  const deleteStore = () => {
    deleteStoreById(detail.id)
      .then((response) => {
        Alert.alert('Berhasil delete data', '', [
          {
            text: 'Ok',
            onPress: () => {
              navigation.navigate(screenNames.list, {
                refresh: true,
              });
            },
          },
        ]);
      })
      .catch((error) => {
        Alert.alert('Error');
      });
  };

  const onDeletePreessed = () => {
    Alert.alert('Delete?', 'data yang dihapus tidak mungkin kembali', [
      {
        text: 'Ok',
        onPress: deleteStore,
      },
      {
        text: 'Cancel',
      },
    ]);
  };

  const onEditPreessed = () => {
    navigation.navigate(screenNames.edit);
  };

  const onGenerateBarcodePressed = () => {
    navigation.navigate(screenNames.barcodePreview, {
      value: JSON.stringify({name: detail.name, initial: detail.initial}),
    });
  };

  return (
    <AppContainer
      containerStyle={{
        justifyContent: 'space-between',
      }}>
      <Card style={{maxHeight: 300}}>
        <Card.Content>
          <Title>{detail.name}</Title>
          <Paragraph>{detail.initial}</Paragraph>
          <Paragraph>{detail.address}</Paragraph>
          <Paragraph>{detail.city}</Paragraph>
        </Card.Content>
      </Card>
      <View style={{width: '100%'}}>
        <AppButton
          mode="contained"
          style={{
            backgroundColor: DefaultTheme.colors.success,
          }}
          loading={loading}
          disabled={loading}
          onPress={onGenerateBarcodePressed}>
          Generate Barcode
        </AppButton>
        <AppButton
          mode="contained"
          style={{
            backgroundColor: DefaultTheme.colors.accent,
          }}
          loading={loading}
          disabled={loading}
          onPress={onEditPreessed}>
          Edit
        </AppButton>
        <AppButton
          mode="contained"
          style={{
            backgroundColor: 'red',
          }}
          loading={loading}
          disabled={loading}
          onPress={onDeletePreessed}>
          Delete
        </AppButton>
      </View>
    </AppContainer>
  );
}
