import {connect} from 'react-redux';
import {deleteStoreById} from '../../features/StoreListRedux/redux/actions';
import Screen from './screen';

const mapStateToProps = (state) => ({
  detail: state.store.detail,
  loading: state.store.deleteLoading,
});

const mapDispatchToProps = (dispatch) => ({
  deleteStoreById: (id) => dispatch(deleteStoreById(id)),
});

const withRedux = connect(mapStateToProps, mapDispatchToProps);

const StoreDetailScreen = withRedux(Screen);

export default StoreDetailScreen;
