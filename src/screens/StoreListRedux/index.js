import {connect} from 'react-redux';
import {
  getListStore,
  setStoreDetail,
} from '../../features/StoreListRedux/redux/actions';
import Screen from './screen';

const mapStateToProps = (state) => ({
  list: state.store.list,
  loading: state.store.listLoading,
});

const mapDispatchToProps = (dispatch) => ({
  getListStore: () => dispatch(getListStore()),
  setDetail: (detail) => dispatch(setStoreDetail(detail)),
});

const withRedux = connect(mapStateToProps, mapDispatchToProps);

const StoreListScreen = withRedux(Screen);

export default StoreListScreen;
