import React, {useLayoutEffect, useState, useEffect, useCallback} from 'react';
import {Alert, FlatList} from 'react-native';
import {FAB, IconButton, List} from 'react-native-paper';
import {AppContainer, AppSearchForm} from '../../components';
import {keyExtractor, refreshControl} from '../../utils/flatlist';
import {screenSizes} from '../../utils/style';

import screenNames from '../../features/StoreListRedux/navigation/screenNames';

export default function Screen({
  route,
  navigation,
  getListStore,
  list,
  loading,
  setDetail,
}) {
  const [searchResult, setSearchResult] = useState([]);
  const [searchText, setSearchText] = useState('');

  useEffect(() => {
    getListStore();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    const refresh = route?.params?.refresh;

    if (refresh) {
      setSearchText('');
    }
  }, [navigation, route.params.refresh]);

  useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <IconButton icon="plus" size={32} onPress={onLinkToAdd} color="white" />
      ),
    });
  }, [navigation, onLinkToAdd]);

  const onLinkToAdd = useCallback(() => {
    navigation.navigate(screenNames.add);
  }, [navigation]);

  const onItemPressed = (item) => () => {
    navigation.navigate(screenNames.detail);
    setDetail(item);
  };

  return (
    <>
      <AppContainer
        containerStyle={{
          paddingHorizontal: 0,
        }}>
        <AppSearchForm
          setSearchResult={setSearchResult}
          setSearchText={setSearchText}
          searchText={searchText}
          searchFields={['name', 'initial']}
          placeholder="Search Store"
          list={list}
        />
        <FlatList
          data={searchText.length > 0 ? searchResult : list}
          keyExtractor={keyExtractor('store')}
          refreshControl={refreshControl(loading, getListStore)}
          renderItem={({item}) => (
            <List.Item
              style={{width: screenSizes.width}}
              title={item.name}
              onPress={onItemPressed(item)}
              description={item.initial}
              left={() => <IconButton icon="store" size={32} />}
            />
          )}
        />
      </AppContainer>
      <FAB
        onPress={onLinkToAdd}
        icon="plus"
        style={{
          position: 'absolute',
          bottom: 10,
          right: 10,
        }}
      />
    </>
  );
}
