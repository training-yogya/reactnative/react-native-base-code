import {connect} from 'react-redux';
import {updateStoreById} from '../../features/StoreListRedux/redux/actions';
import Screen from './screen';

const mapStateToProps = (state) => ({
  detail: state.store.detail,
  loading: state.store.editLoading,
});

const mapDispatchToProps = (dispatch) => ({
  editStoreById: (id, data) => dispatch(updateStoreById(id, data)),
});

const withRedux = connect(mapStateToProps, mapDispatchToProps);

const StoreEditScreen = withRedux(Screen);

export default StoreEditScreen;
