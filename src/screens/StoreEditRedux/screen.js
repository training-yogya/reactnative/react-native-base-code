import React from 'react';
import {Alert} from 'react-native';
import {AppContainer} from '../../components';
import screenNames from '../../features/StoreListRedux/navigation/screenNames';

import StoreEditForm from './components/StoreEditForm';

export default function Screen({
  navigation,
  route,
  editStoreById,
  detail,
  loading,
}) {
  // const onSubmit = ({name, address, city}) => {
  const onSubmit = (form) => {
    editStoreById(detail.id, form)
      .then((response) => {
        Alert.alert('Berhasil Edit data', '', [
          {
            text: 'Ok',
            onPress: () => {
              navigation.navigate(screenNames.detail);
            },
          },
        ]);
      })
      .catch((error) => {
        Alert.alert('Error');
      });
  };

  return (
    <AppContainer>
      <StoreEditForm
        onSubmit={onSubmit}
        loading={loading}
        defaultValue={detail}
      />
    </AppContainer>
  );
}
