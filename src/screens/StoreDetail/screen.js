import React, {useLayoutEffect, useState} from 'react';
import {Alert, View} from 'react-native';
import {Card, IconButton, Paragraph, Title} from 'react-native-paper';
import {AppButton, AppContainer} from '../../components';
import screenNames from '../../features/StoreList/navigation/screenNames';
import {DefaultTheme} from '../../theme';
import {axiosClient} from '../../utils/axios';

export default function Screen({route, navigation}) {
  const storeData = route.params.storeData;
  const edited = route.params.edited || false;

  const [loading, setLoading] = useState(false);

  const deleteStore = () => {
    setLoading(true);
    axiosClient
      .delete('/store/' + storeData.id)
      .then((response) => {
        Alert.alert('Berhasil delete data', '', [
          {
            text: 'Ok',
            onPress: () => {
              navigation.navigate(screenNames.list, {
                refresh: true,
              });
            },
          },
        ]);
      })
      .catch((error) => {
        Alert.alert('Error');
      })
      .finally(() => {
        setLoading(false);
      });
  };

  const onDeletePreessed = () => {
    Alert.alert('Delete?', 'data yang dihapus tidak mungkin kembali', [
      {
        text: 'Ok',
        onPress: deleteStore,
      },
      {
        text: 'Cancel',
      },
    ]);
  };

  const onEditPreessed = () => {
    navigation.navigate(screenNames.edit, {
      storeData,
    });
  };

  const onBackPressed = () => {
    if (edited) {
      navigation.navigate(screenNames.list, {
        refresh: true,
      });
    } else {
      navigation.goBack();
    }
  };

  const onGenerateBarcodePressed = () => {
    navigation.navigate(screenNames.barcodePreview, {
      value: JSON.stringify({name: storeData.name, initial: storeData.initial}),
    });
  };

  useLayoutEffect(() => {
    navigation.setOptions({
      headerLeft: () => (
        <IconButton
          icon="arrow-left"
          size={25}
          color={'white'}
          onPress={onBackPressed}
        />
      ),
    });
  });

  return (
    <AppContainer
      containerStyle={{
        justifyContent: 'space-between',
      }}>
      <Card style={{maxHeight: 300}}>
        <Card.Content>
          <Title>{storeData.name}</Title>
          <Paragraph>{storeData.initial}</Paragraph>
          <Paragraph>{storeData.address}</Paragraph>
          <Paragraph>{storeData.city}</Paragraph>
        </Card.Content>
      </Card>
      <View style={{width: '100%'}}>
        <AppButton
          mode="contained"
          style={{
            backgroundColor: DefaultTheme.colors.success,
          }}
          onPress={onGenerateBarcodePressed}>
          Generate Barcode
        </AppButton>
        <AppButton
          mode="contained"
          style={{
            backgroundColor: DefaultTheme.colors.accent,
          }}
          onPress={onEditPreessed}>
          Edit
        </AppButton>
        <AppButton
          mode="contained"
          style={{
            backgroundColor: 'red',
          }}
          onPress={onDeletePreessed}>
          Delete
        </AppButton>
      </View>
    </AppContainer>
  );
}
