export default {
  index: 'storelistredux/INDEX',
  list: 'storelistredux/LIST',
  detail: 'storelistredux/DETAIL',
  edit: 'storelistredux/EDIT',
  add: 'storelistredux/ADD',
  barcodePreview: 'storeListredux/BARCODE_PREVIEW',
};
