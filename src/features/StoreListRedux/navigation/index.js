import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import screenNames from './screenNames';
import StoreListScreen from '../../../screens/StoreListRedux';
import StoreDetailScreen from '../../../screens/StoreDetailRedux';
import {DefaultTheme} from '../../../theme';
import StoreEditScreen from '../../../screens/StoreEditRedux';
import StoreAddScreen from '../../../screens/StoreAddRedux';
import BarcodePreviewScreen from '../../../screens/BarcodePreview';

const {Navigator, Screen} = createStackNavigator();

const StoreListReduxStack = () => {
  return (
    <Navigator
      screenOptions={{
        headerShown: true,
        headerTintColor: DefaultTheme.colors.white,
        headerStyle: {
          backgroundColor: DefaultTheme.colors.primary,
        },
      }}>
      <Screen
        options={{
          title: 'Store List',
        }}
        initialParams={{
          refresh: false,
        }}
        name={screenNames.list}
        component={StoreListScreen}
      />
      <Screen
        options={{
          title: 'Store Detail',
        }}
        name={screenNames.detail}
        component={StoreDetailScreen}
      />

      <Screen
        options={{
          title: 'Store Edit',
        }}
        name={screenNames.edit}
        component={StoreEditScreen}
      />
      <Screen
        options={{
          title: 'Store Add',
        }}
        name={screenNames.add}
        component={StoreAddScreen}
      />
      <Screen
        options={{
          title: 'Store Barcode',
        }}
        name={screenNames.barcodePreview}
        component={BarcodePreviewScreen}
      />
    </Navigator>
  );
};

export default StoreListReduxStack;
