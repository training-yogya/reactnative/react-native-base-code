import * as actionTypes from './constant';
import {initialState} from './state';

export default function storeReducer(state = initialState, action) {
  switch (action.type) {
    case actionTypes.GET_LIST_PENDING:
      return {
        ...state,
        listLoading: true,
      };
    case actionTypes.GET_LIST_SUCCESS:
      return {
        ...state,
        listLoading: false,
        list: action.payload,
      };
    case actionTypes.GET_LIST_FAILED:
      return {
        ...state,
        listLoading: false,
      };
    case actionTypes.ADD_PENDING:
      return {
        ...state,
        addLoading: true,
      };
    case actionTypes.ADD_SUCCESS:
      return {
        ...state,
        addLoading: false,
      };
    case actionTypes.ADD_FAILED:
      return {
        ...state,
        addLoading: false,
      };
    case actionTypes.UPDATE_PENDING:
      return {
        ...state,
        updateLoading: true,
      };
    case actionTypes.UPDATE_SUCCESS:
      return {
        ...state,
        updateLoading: false,
      };
    case actionTypes.UPDATE_FAILED:
      return {
        ...state,
        updateLoading: false,
      };
    case actionTypes.DELETE_PENDING:
      return {
        ...state,
        deleteLoading: true,
      };
    case actionTypes.DELETE_SUCCESS:
      return {
        ...state,
        deleteLoading: false,
      };
    case actionTypes.DELETE_FAILED:
      return {
        ...state,
        deleteLoading: false,
      };
    case actionTypes.SET_DETAIL:
      return {
        ...state,
        detail: action.payload,
      };
    default:
      return state;
  }
}
