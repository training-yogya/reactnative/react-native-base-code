export const initialState = {
  list: [],
  detail: {},
  listLoading: false,
  updateLoading: false,
  addLoading: false,
  deleteLoading: false,
};
