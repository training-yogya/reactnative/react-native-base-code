export const GET_LIST_PENDING = 'storelistredux/GET_LIST_PENDING';
export const GET_LIST_SUCCESS = 'storelistredux/GET_LIST_SUCCESS';
export const GET_LIST_FAILED = 'storelistredux/GET_LIST_FAILED';

export const SET_DETAIL = 'storelistredux/SET_DETAIL';

export const UPDATE_PENDING = 'storelistredux/UPDATE_PENDING';
export const UPDATE_SUCCESS = 'storelistredux/UPDATE_SUCCESS';
export const UPDATE_FAILED = 'storelistredux/UPDATE_FAILED';

export const ADD_PENDING = 'storelistredux/ADD_PENDING';
export const ADD_SUCCESS = 'storelistredux/ADD_SUCCESS';
export const ADD_FAILED = 'storelistredux/ADD_FAILED';

export const DELETE_PENDING = 'storelistredux/DELETE_PENDING';
export const DELETE_SUCCESS = 'storelistredux/DELETE_SUCCESS';
export const DELETE_FAILED = 'storelistredux/DELETE_FAILED';
