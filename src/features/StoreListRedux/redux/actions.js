import {axiosClient} from '../../../utils/axios';
import * as actionTypes from './constant';

export const setStoreDetail = (detail) => {
  return async (dispatch) => {
    dispatch({
      type: actionTypes.SET_DETAIL,
      payload: detail,
    });
  };
};

export const getListStore = () => {
  return async (dispatch) => {
    dispatch({
      type: actionTypes.GET_LIST_PENDING,
    });
    try {
      const listResponse = await axiosClient.get('/store');

      const listStore = listResponse.data.data;

      dispatch({
        type: actionTypes.GET_LIST_SUCCESS,
        payload: listStore,
      });
    } catch (error) {
      dispatch({
        type: actionTypes.GET_LIST_SUCCESS,
        payload: error.response || error,
      });
    }
  };
};

export const updateStoreById = (id, body) => {
  return async (dispatch) => {
    dispatch({
      type: actionTypes.UPDATE_PENDING,
    });

    const data = new FormData();

    Object.keys(body).forEach((formkey) => {
      data.append(formkey, body[formkey]);
    });

    try {
      await axiosClient.put('/store/' + id, data);

      dispatch({
        type: actionTypes.UPDATE_SUCCESS,
      });

      dispatch(setStoreDetail(body));
      dispatch(getListStore());

      return Promise.resolve(true);
    } catch (error) {
      dispatch({
        type: actionTypes.UPDATE_FAILED,
        error,
      });
      return Promise.reject(error);
    }
  };
};

export const addStore = (body) => {
  return async (dispatch) => {
    dispatch({
      type: actionTypes.ADD_PENDING,
    });

    const data = new FormData();

    Object.keys(body).forEach((formkey) => {
      data.append(formkey, body[formkey]);
    });

    try {
      const addResponse = await axiosClient.post('/store/', data);

      dispatch({
        type: actionTypes.ADD_SUCCESS,
      });

      dispatch(getListStore());

      const message = addResponse.data.status.message;
      return Promise.resolve(message);
    } catch (error) {
      dispatch({
        type: actionTypes.ADD_FAILED,
        error,
      });
      const message = error?.response?.data?.status?.message ?? 'Gagal';
      return Promise.reject(new Error(message));
    }
  };
};

export const deleteStoreById = (id) => {
  return async (dispatch) => {
    dispatch({
      type: actionTypes.DELETE_PENDING,
    });

    try {
      await axiosClient.delete('/store/' + id);

      dispatch({
        type: actionTypes.DELETE_SUCCESS,
      });
      dispatch(getListStore());
      return Promise.resolve(true);
    } catch (error) {
      dispatch({
        type: actionTypes.DELETE_FAILED,
      });
      return Promise.reject(error);
    }
  };
};
